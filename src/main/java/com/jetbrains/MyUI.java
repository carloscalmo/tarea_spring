package com.jetbrains;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.Editor;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.components.grid.SingleSelectionModel;
import javafx.scene.control.SelectionMode;
import org.springframework.beans.factory.annotation.Autowired;
import sun.misc.FormattedFloatingDecimal;

import java.util.concurrent.ThreadFactory;




@Theme("mytheme")
public class MyUI extends UI {

    //CREACIÓN DEL OBJETO REPOSITORY PARA ALMACENAMIENTO
    @Autowired
    RepositoryContacto repoContacto;

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        //DECLARACIÓN DE VARIABLES DE USO
        Label titulo = new Label("Agenda Electrónica");
        titulo.addStyleName("h1");

        VerticalLayout alineacionPrincipal = new VerticalLayout();
        HorizontalLayout alineacionForm = new HorizontalLayout();
        HorizontalLayout alineacionBotones = new HorizontalLayout();
        FormLayout formDatos; formDatos = new FormLayout();
        TextField fieldNombre, fieldDireccion, fieldTelefono, fieldEmail;



        //Grid para presentar la lista de contactos
        Grid<Contacto> gridContactos = new Grid<>();
        //gridContactos.addColumn(Contacto::getId).setCaption("ID");
        gridContactos.addColumn(Contacto::getNombre).setCaption("Nombre");
        gridContactos.addColumn(Contacto::getDireccion).setCaption("Dirección");
        gridContactos.addColumn(Contacto::getTelefono).setCaption("Teléfono");
        gridContactos.addColumn(Contacto::getEmail).setCaption("Correo Electrónico");
        gridContactos.setDescription("Haga doble cilck para editar un contacto");
        gridContactos.setSizeFull();
        //gridContactos.setItems(repoContacto.findAll());


        //Botones para guardar y eliminar contactos
        Button botonGuardar = new Button("Guardar contacto");
        Button botonEliminar = new Button("Eliminar contacto");
        alineacionBotones.addComponent(botonGuardar);
        alineacionBotones.addComponent(botonEliminar);

        //FORM LAYOUT - CAPTURAR DATOS
        fieldNombre = new TextField("Nombre");
        fieldNombre.setVisible(true);
        fieldNombre.setIcon(VaadinIcons.USER);
        fieldNombre.setRequiredIndicatorVisible(true);
        formDatos.addComponent(fieldNombre);

        fieldDireccion = new TextField("Dirección");
        fieldDireccion.setVisible(true);
        fieldDireccion.setIcon(VaadinIcons.HOME);
        formDatos.addComponent(fieldDireccion);

        fieldTelefono = new TextField("Teléfono");
        fieldTelefono.setVisible(true);
        fieldTelefono.setIcon(VaadinIcons.PHONE);
        formDatos.addComponent(fieldTelefono);

        fieldEmail = new TextField("Correo Electrónico");
        fieldEmail.setVisible(true);
        fieldEmail.setIcon(VaadinIcons.ENVELOPE);
        formDatos.addComponent(fieldEmail);



        //ACCIÓN DE LOS BOTONES
        //botón guardar
        botonGuardar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Contacto objetoContacto = new Contacto();
                objetoContacto.setNombre(fieldNombre.getValue());
                objetoContacto.setDireccion(fieldDireccion.getValue());
                objetoContacto.setEmail(fieldEmail.getValue());
                objetoContacto.setTelefono(fieldTelefono.getValue());
                repoContacto.save(objetoContacto);

                fieldNombre.clear();
                fieldDireccion.clear();
                fieldTelefono.clear();
                fieldEmail.clear();
                Notification.show("El contacto ha sido añadido.");
                gridContactos.setItems(repoContacto.findAll());
            }
        });

        //botón eliminar
        botonEliminar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {

            }
        });



        //alineación horizontal
        alineacionForm.addComponent(formDatos);
        alineacionForm.setComponentAlignment(formDatos, Alignment.MIDDLE_CENTER);



        //alineación vertical
        alineacionPrincipal.addComponent(titulo);
        alineacionPrincipal.setComponentAlignment(titulo, Alignment.TOP_LEFT);
        alineacionPrincipal.addComponent(alineacionForm);
        alineacionPrincipal.setComponentAlignment(alineacionForm, Alignment.MIDDLE_CENTER);
        alineacionPrincipal.addComponent(alineacionBotones);
        alineacionPrincipal.setComponentAlignment(alineacionBotones, Alignment.TOP_RIGHT);
        alineacionPrincipal.addComponent(gridContactos);
        alineacionPrincipal.setComponentAlignment(gridContactos, Alignment.MIDDLE_CENTER);
        alineacionPrincipal.setVisible(true);

        setContent(alineacionPrincipal);




























    }


    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
